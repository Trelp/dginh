import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        ArrayList<Money> list1 = new ArrayList<Money>(4);
        list1.add(new Money(19, 89));
        list1.add(new Money(21, 79));
        list1.add(new Money(21, 77));
        list1.add(new Money(39, 83));

        for (Money m : list1) {
            System.out.println(m);
        }

        System.out.println();
        Collections.sort(list1);

        for (Money m : list1) {
            System.out.println(m);
        }

        ArrayList<Complex> list2 = new ArrayList<Complex>(4);
        list2.add(new Complex(1, 8));
        list2.add(new Complex(2, 9));
        list2.add(new Complex(1, 7));
        list2.add(new Complex(3, 3));

        for (Complex c : list2) {
            System.out.println(c);
        }

        System.out.println();
        Collections.sort(list2);

        for (Complex c : list2) {
            System.out.println(c);
        }
    }
}
