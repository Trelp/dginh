/**
 * Created with IntelliJ IDEA.
 * User: Trelp
 * Date: 21.03.13
 * Time: 2:17
 * To change this template use File | Settings | File Templates.
 */
public class Complex extends PairDouble implements Comparable {

    Complex() {
        super();
    }

    Complex(double x, double y) {
        super(x, y);
    }

    Complex(PairDouble p) {
        super(p);
    }

    @Override
    public String toString() {
        return "x = " + x + "\t\ty = " + y;
    }

    @Override
    public int compareTo(Object obj) {
        Complex c = (Complex) obj;
        return (x == c.x && y == c.y) ? 0 : ((x == c.x) ? ((y > c.y) ? 1 : -1) : ((x > c.x) ? 1 : -1));
    }

    public Complex multipl(Complex c) {
        return new Complex(x * c.x - y * c.y, x * c.y + y * c.x);
    }

    public Complex subComplex(Complex c) {
        return new Complex(x - y, c.x - c.y);
    }
}