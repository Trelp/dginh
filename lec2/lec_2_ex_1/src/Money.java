/**
 * Created with IntelliJ IDEA.
 * User: Trelp
 * Date: 21.03.13
 * Time: 0:56
 * To change this template use File | Settings | File Templates.
 */
public class Money extends Pair implements Comparable {

    private int ruble;
    private int penny;

    public Money() {
        ruble = 0;
        penny = 0;
    }

    public Money(int rub, int pen) {
        ruble = rub;
        penny = pen;
    }

    public Money(Pair p) {
        ruble = p.x;
        penny = p.y;
    }

    @Override
    public int compareTo(Object obj) {
        Money m = (Money) obj;
        return (ruble * 100 + penny == m.ruble * 100 + m.penny) ? 0 :
                ((ruble * 100 + penny < m.ruble * 100 + m.penny) ? 1 : -1);
    }

    @Override
    public String toString() {
        return "ruble = " + ruble + "\tpenny = " + penny;
    }

    @Override
    public Pair addPair(Pair p) {
        Money temp = new Money(p);
        temp.penny += penny + (ruble + temp.ruble) * 100;
        if (temp.penny >= 100) {
            temp.ruble = temp.penny / 100;
            temp.penny %= 100;
        }
        return temp;
    }

    public Money subMoney(Money m) {
        return new Money(ruble - m.ruble, penny - m.penny);
    }

    public Money divMoney(int number) {
        return new Money(ruble / number, penny / number);
    }
}
