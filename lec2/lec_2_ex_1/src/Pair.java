/**
 * Created with IntelliJ IDEA.
 * User: Trelp
 * Date: 21.03.13
 * Time: 0:47
 * To change this template use File | Settings | File Templates.
 */
public class Pair {

    protected int x;
    protected int y;

    Pair() {
        x = 0;
        y = 0;
    }

    Pair(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    Pair(Pair p) {
        x = p.x;
        y = p.y;
    }

    @Override
    public String toString() {
        return "x = " + x + "\ty = " + y;
    }

    public Pair multipl(int number) {
        return new Pair(x * number, y * number);
    }

    public Pair addPair(Pair p) {
        return new Pair(x + p.x, y + p.y);
    }
}
