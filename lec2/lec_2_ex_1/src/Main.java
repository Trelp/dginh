import java.util.ArrayList;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {
        Pair p1 = new Pair(1, 4);
        Pair p2 = new Pair(p1);
        Pair p3 = p1.addPair(p2);
        System.out.println(p3);

        Money m1 = new Money(19, 89);
        Pair m2 = new Pair(4, 33);
        Pair m3 = m1.addPair(m2);
        System.out.println(m3);

        Complex c1 = new Complex(4.5, 3.0);
        Complex c2 = new Complex(1.0, 0.5);
        Complex c3 = c1.multipl(c2);
        Complex c4 = c1.subComplex(c2);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        System.out.println(c4);

        ArrayList<Money> list = new ArrayList<Money>(4);
        list.add(new Money(19, 89));
        list.add(new Money(21, 79));
        list.add(new Money(21, 77));
        list.add(new Money(39, 83));

        for (Money m : list) {
            System.out.println(m);
        }

        System.out.println();
        Collections.sort(list);

        for (Money m : list) {
            System.out.println(m);
        }
    }
}
