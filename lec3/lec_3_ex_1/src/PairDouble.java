/**
 * Created with IntelliJ IDEA.
 * User: Trelp
 * Date: 27.03.13
 * Time: 21:04
 * To change this template use File | Settings | File Templates.
 */
public class PairDouble {

    protected double x;
    protected double y;

    PairDouble() {
        x = 0.0;
        y = 0.0;
    }

    PairDouble(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    PairDouble(PairDouble p) {
        x = p.x;
        y = p.y;
    }

    @Override
    public String toString() {
        return "x = " + x + "\ny = " + y;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof PairDouble ? (this.x == ((PairDouble) obj).x && this.y == ((PairDouble) obj).y)  : false);
    }

    /*@Override
    public ? hashCode() {
        return ?;
    }*/

    public PairDouble multipl(int number) {
        return new PairDouble(x * number, y * number);
    }

    public PairDouble addPair(PairDouble p) {
        return new PairDouble(x + p.x, y + p.y);
    }
}
