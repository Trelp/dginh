public class Main {

    public static void main(String[] args) {
        PairDouble pd1 = new PairDouble(5.8, 8.9);
        PairDouble pd2 = new PairDouble(5.7, 7.0);
        PairDouble pd3 = new PairDouble(5.8, 8.9);
        System.out.println(pd1.equals(pd2) + "\t" + pd1.equals(pd3));

        Complex c1 = new Complex(5.8, 8.9);
        Complex c2 = new Complex(5.7, 7.0);
        Complex c3 = new Complex(5.8, 8.9);
        System.out.println(c1.equals(c3) + "\t" + c2.equals(c3));
    }
}
