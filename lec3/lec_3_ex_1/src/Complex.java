/**
 * Created with IntelliJ IDEA.
 * User: Trelp
 * Date: 27.03.13
 * Time: 21:07
 * To change this template use File | Settings | File Templates.
 */
public class Complex extends PairDouble {

    Complex() {
        super();
    }

    Complex(double x, double y) {
        super(x, y);
    }

    Complex(PairDouble p) {
        super(p);
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Complex ? (this.x == ((Complex) obj).x && this.y == ((Complex) obj).y)  : false);
    }

    @Override
    public String toString() {
        return "x = " + x + "\ty = " + y;
    }

    /*@Override
    public ? hashCode() {
        return ?;
    }*/

    public Complex multipl(Complex c) {
        return new Complex(x * c.x - y * c.y, x * c.y + y * c.x);
    }

    public Complex subComplex(Complex c) {
        return new Complex(x - y, c.x - c.y);
    }
}