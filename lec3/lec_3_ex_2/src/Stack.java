/**
 * Created with IntelliJ IDEA.
 * User: Trelp
 * Date: 27.03.13
 * Time: 15:19
 * To change this template use File | Settings | File Templates.
 */
public class Stack {
    private int top;
    private int[] mas;

    Stack(int capacity) {
        mas = new int[capacity];
    }

    public void push(int value) {
        if (top == mas.length) {
            System.out.println("Стек полон!");
        } else {
            mas[top++] = value;
        }
    }

    public int pop() {
        return mas[--top];
    }
}
