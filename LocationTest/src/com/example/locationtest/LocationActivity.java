package com.example.locationtest;

import android.app.Activity;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class LocationActivity extends Activity implements OnClickListener {

	private Button btnLocation;
	private TextView tvLocation;
	private LocationManager locManager;
	private LocationListener locListener;

	private class GpsTest implements LocationListener {

		@Override
		public void onLocationChanged(Location location) {
			// TODO Auto-generated method stub
			String str = "������: " + location.getLatitude() + "\n�������: "
					+ location.getLongitude();
			tvLocation.setText(str);
		}

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			Toast.makeText(getApplicationContext(), "Gps ��������",
					Toast.LENGTH_LONG).show();
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			Toast.makeText(getApplicationContext(), "Gps �������",
					Toast.LENGTH_LONG).show();
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			// TODO Auto-generated method stub

		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.location);

		// ��������� �������� � ������
		locManager = (LocationManager) getSystemService(LOCATION_SERVICE);

		// ����������� ���������� ��������� ������ ������
		locListener = new GpsTest();

		// ������� View-��������
		tvLocation = (TextView) findViewById(R.id.tvLocation);
		btnLocation = (Button) findViewById(R.id.btnLocation);

		// �������� ������������ ���� ��� tvLocation
		registerForContextMenu(tvLocation);

		// ����������� Button ����������
		btnLocation.setOnClickListener(this);
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.contextmenu, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.menu_red:
			tvLocation.setTextColor(Color.RED);
			break;
		case R.id.menu_green:
			tvLocation.setTextColor(Color.GREEN);
			break;
		case R.id.menu_blue:
			tvLocation.setTextColor(Color.BLUE);
			break;
		}
		return super.onContextItemSelected(item);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// ��������� ������������
		locManager.removeUpdates(locListener);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		// ��������� ������������
		locManager.removeUpdates(locListener);
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		// ��������� ���������(������� ������������)
		locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
				locListener);
	}

}
