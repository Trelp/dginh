package com.example.locationtest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;

public class MainActivity extends Activity {

	CheckBox chBox;
	Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		// ������� �������
		chBox = (CheckBox) findViewById(R.id.checkBox1);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		// ������ ���� � id = menu_expanded �����, ���� isChecked ����������
		// true
		menu.setGroupVisible(R.id.menu_expanded, chBox.isChecked());
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		switch (item.getItemId()) {
		case R.id.menu_compass:
			Intent intent = new Intent(this, CompassActivity.class);
			startActivity(intent);
			break;
		case R.id.menu_location:
			intent = new Intent(this, LocationActivity.class);
			startActivity(intent);
			break;
		case R.id.menu_converter:
			intent = new Intent(this, ConverterActivity.class);
			startActivity(intent);
			break;
		case R.id.menu_settings:
			break;
		case R.id.menu_exit:
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}