package com.example.intentstest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends Activity implements OnClickListener {

	final int PICK_RESULT = 1;
	final int CAMERA_RESULT = 2;

	Button btnDialOrCall, btnWeb, btnSms, btnMap, btnContact, btnContacts,
			btnEmail, btnCamera;
	EditText edtPhone, edtWeb, edtEmail;
	CheckBox chbDialOrCall;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		chbDialOrCall = (CheckBox) findViewById(R.id.chbDialOrCall);

		edtPhone = (EditText) findViewById(R.id.edtPhone);
		edtWeb = (EditText) findViewById(R.id.edtWeb);
		edtEmail = (EditText) findViewById(R.id.edtEmail);

		btnDialOrCall = (Button) findViewById(R.id.btnDialOrCall);
		btnWeb = (Button) findViewById(R.id.btnWeb);
		btnSms = (Button) findViewById(R.id.btnSms);
		btnMap = (Button) findViewById(R.id.btnMap);
		btnEmail = (Button) findViewById(R.id.btnEmail);
		btnContact = (Button) findViewById(R.id.btnContact);
		btnContacts = (Button) findViewById(R.id.btnContacts);
		btnCamera = (Button) findViewById(R.id.btnCamera);

		btnDialOrCall.setOnClickListener(this);
		btnWeb.setOnClickListener(this);
		btnSms.setOnClickListener(this);
		btnMap.setOnClickListener(this);
		btnContact.setOnClickListener(this);
		btnContacts.setOnClickListener(this);
		btnEmail.setOnClickListener(this);
		btnCamera.setOnClickListener(this);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == CAMERA_RESULT) {
			Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
			ImageView ivCamera = (ImageView) findViewById(R.id.ivCamera);
			ivCamera.setImageBitmap(thumbnail);
		} else if (requestCode == PICK_RESULT) {
			if (resultCode == RESULT_OK) {
				startActivity(new Intent(Intent.ACTION_VIEW, data.getData()));
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent intent = new Intent();
		switch (v.getId()) {
		case R.id.btnDialOrCall:
			intent.setData(Uri.parse("tel:" + edtPhone.getText().toString()));
			// true = CALL, false = VIEW
			if (chbDialOrCall.isChecked()) {
				intent.setAction(Intent.ACTION_CALL);
			} else {
				intent.setAction(Intent.ACTION_VIEW);
			}
			// ������� ����� ����������� ��������
			startActivity(intent);
			// ��������� ���� ��� �����
			edtPhone.setText(null);
			break;
		case R.id.btnMap:
			intent.setAction(Intent.ACTION_VIEW);
			intent.setData(Uri
					.parse("geo: 42.96179068736895,47.497628781058346"));
			startActivity(intent);
			break;
		case R.id.btnSms:
			intent.setAction(Intent.ACTION_VIEW);
			intent.setType("vnd.android-dir/mms-sms");
			intent.putExtra("sms_body", "Some SMS text");
			startActivity(intent);

			break;
		case R.id.btnWeb:
			intent.setData(Uri.parse("http://" + edtWeb.getText().toString()));
			intent.setAction(Intent.ACTION_VIEW);
			startActivity(intent);
			edtWeb.setText(null);
			break;
		case R.id.btnContact:
			intent.setAction(Intent.ACTION_PICK);
			intent.setData(android.provider.ContactsContract.Contacts.CONTENT_URI);
			startActivityForResult(intent, PICK_RESULT);
			break;
		case R.id.btnContacts:
			intent.setAction(Intent.ACTION_GET_CONTENT);
			intent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
			startActivityForResult(intent, PICK_RESULT);
			break;
		case R.id.btnEmail:
			intent.setAction(Intent.ACTION_SEND);
			intent.setType("text/plain");
			intent.putExtra(Intent.EXTRA_TEXT, edtEmail.getText().toString());
			startActivity(Intent.createChooser(intent, "Sharing something"));
			break;
		case R.id.btnCamera:
			intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
			startActivityForResult(intent, CAMERA_RESULT);
			break;
		}
	}
}