import java.io.*;

public class PrimeNumber {
	public static void main(String args []) throws NumberFormatException, IOException {
		System.out.print("������� �����: ");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int count = Integer.parseInt(br.readLine());
		if (count == 1) {
			System.out.println("����� " + count + " �� �������� �������!");
			return;
		}
		int i = 1;
		while (count%++i != 0) {}
		if (i == count) {
			System.out.println("����� " + count + " �������!");
		} else {
			System.out.println("����� " + count + " ���������!");
		}
	}
}
