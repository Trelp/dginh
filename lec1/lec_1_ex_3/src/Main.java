import java.io.*;

public class Main {

    public static void main(String[] args) {
        System.out.println("Введите значения коэффициентов квадратного уравнения(a != 0, b, c): ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            int a = Integer.parseInt(br.readLine());
            if (a == 0) {
                System.out.println("Попытка деления на 0!");
                return;
            }
            int b = Integer.parseInt(br.readLine());
            int c = Integer.parseInt(br.readLine());
            double d = Math.pow(b, 2) - 4 * a * c;
            if (d < 0) {
                System.out.println("Корней нет!");
            } else if (d > 0) {
                double x1 = (-b - Math.sqrt(d)) / (2 * a);
                double x2 = (-b + Math.sqrt(d)) / (2 * a);
                System.out.println("x1 = " + x1 + "\nx2 = " + x2);
            } else {
                double x = -b / (2 * a);
                System.out.println("Единственный корень: x = " + x);
            }
        } catch (IOException ex) {
            System.out.println(ex);
        } finally {
            try {
                br.close();
            } catch (IOException ex) {
                System.out.println("Ошибка закрытия!");
            }
        }
    }
}
