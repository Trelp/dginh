import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Введите последовательность целых чисел через пробел(завершает последовательность 0)");
        Scanner sc = new Scanner(System.in);
        int count, sum = 0;
        do {
            if (sc.hasNextInt()) {
                count = sc.nextInt();
                sum += count;
            } else {
                count = 0;
            }
        } while (count != 0);
        System.out.println(sum);
    }
}
